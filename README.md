# ISO-5457 Worksheets for KiCad E.D.A.

## Description ##

Due to the lack of ISO-5457 compliant worksheets for KiCad, I decided to make my
own. This is actually a work in progress, things may change.

I will be adding more formats, but my personal printer is only A4, so that is
for the moment.

![](https://i.imgur.com/ZSItVTv.jpg)

`worksheet_iso_5457_a4_portrait.kicad_wks`

## Author ##

Ángel M. García

## License ##

This work is licensed under the Creative Commons BY-NC-SA 3.0 Unported License. 
To view a copy of this license, visit <http://creativecommons.org/licenses/by-nc-sa/3.0/>
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.